#!/usr/bin/make -f
# -*- makefile -*-

# Uncomment this to turn on verbose mode.
#export DH_VERBOSE=1

export DEB_BUILD_MAINT_OPTIONS=hardening=+all

pkgdata:=microbiomeutil-data
sampledir:=$(CURDIR)/debian/$(pkgdata)/usr/share/doc/$(pkgdata)/examples

%:
	dh $@

override_dh_auto_test:
ifneq (,$(findstring nocheck,$(DEB_BUILD_OPTIONS)))
	echo 'skip testing due to DEB_BUILD_OPTIONS="$(DEB_BUILD_OPTIONS)"'
else
## Run test suite only on powerfull architectures (amd64) at build time
#ifneq (,$(filter $(DEB_HOST_ARCH),amd64 kfreebsd-amd64))
	dh_auto_test
	cd ${CURDIR}/ChimeraSlayer/sample_data && $(MAKE) clean
	cd ${CURDIR}/NAST-iEr/sample_data && $(MAKE) clean
	cd ${CURDIR}/WigeoN/sample_data && $(MAKE) clean
	rm -f RESOURCES/rRNA16S.gold.fasta.n* RESOURCES/*.cidx
	rm -f formatdb.log
#else
#	echo "Do not run test suite when building on architecture $(DEB_HOST_ARCH)"
#endif
endif

override_dh_auto_build:
	dh_auto_build
	#Pre-make indexes for ChimeraSlayer, otherwise it tries to write them
	#to the non-writeable dir on first run and blows up
	formatdb -p F -i RESOURCES/rRNA16S.gold.fasta 2>/dev/null
	cdbfasta -C RESOURCES/rRNA16S.gold.NAST_ALIGNED.fasta

override_dh_auto_clean:
	dh_auto_clean
	rm -f RESOURCES/rRNA16S.gold.fasta.n* RESOURCES/*.cidx
	rm -f formatdb.log

override_dh_install:
	dh_install -XMakefile -Xsample_data -XLICENSE -XNAST-iEr.c
	find debian -name '*.pl' -o -name '*.pm' | xargs sed -i 's|/usr/bin/env perl|/usr/bin/perl|g'

override_dh_installexamples:
	dh_installexamples
	for mod in ChimeraSlayer NAST-iEr WigeoN ; do \
	    mkdir -p $(sampledir)/$${mod} ; \
	    cp -a $${mod}/sample_data/* $(sampledir)/$${mod} ; \
	    sed -i -e "s?../$${mod}?$${mod}?" \
		   -e "s?^.*\(/util/show_malign_no_gap.pl\)?/usr/lib/NAST-iEr\1?" \
		   -e "s?^.*\(/run_NAST-iEr.pl\)?/usr/lib/NAST-iEr\1?" \
		   -e "s?^.*\(ChimeraSlayer.pl\)?/usr/lib/ChimeraSlayer/\1?" \
		   -e "s?^.*\(/run_WigeoN.pl\)?/usr/lib/WigeoN\1?" \
		 $(sampledir)/$${mod}/runMe.sh ; \
	done
	sed -i '1i #!/bin/sh' $(sampledir)/WigeoN/runMe.sh

override_dh_fixperms:
	dh_fixperms
	find debian -type f -name '*.pm' -print0 | xargs -0r chmod a-x
	find debian -type f -name 'genus_counter.pl' -print0 | xargs -0r chmod a+x
